//SE LLAMA A KOA 
const { nunjucks } = require('consolidate');
const Koa = require('koa');
const KoaRouter = require('koa-router');
//Realiza llamado a koa-static para rutear la caperta publica
const static = require('koa-static');
const views = require('koa-views');
const nunJuck = require('nunjucks');
//Llamado a env
require('dotenv').config();
const mongoose = require('mongoose');

//PARA INICIALIZAR
const app = new Koa();
const route = new KoaRouter();
nunJuck.configure('./views', {autoescape: true});
const conexionBaseDato = mongoose.connection;
//ruta para los views renderizados con nunjucks
app.use(views('./views', {
    map: {html: 'nunjucks'}
}));
app.use(route.routes());
app.use(static('./public'))

const user = 'user_mongo_db';
const password = 'trgDN20Ny0dINgNC';
const uri = `mongodb+srv://${user}:${password}@vtoledo.a8snn.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`;
mongoose.connect(uri, function(err, res) {
    if(err) console.log('ERROR: Conectando a la BD: ' + err);
    else console.log('Conexión a la BD realizada');
});

const Movie = require('./models/movie');

route.get('/', (ctx, next) => {
    console.log('conectado a la ruta raiz');
    return Movie.find({}, (error, results) => {
        console.log(results)
        ctx.render('index.html', {
            data: results
        })
    })
})

route.get('/test', ctx => (
    ctx.body = 'Hello Testsssss'
    
));

route.get('/movies/:id', ctx => (
    ctx.body = movies[ctx.params.id]
));


//SE LLAMA AL LISTENER PARA ACTUALIZAR EL SERVIDOR
app.listen(3000, () => console.log('Server Ok...'));