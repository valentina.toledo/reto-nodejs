import {getTitleFromDb} from '../service/movie-service';
import {createTitleInDb} from '../service/movie-service';


static async getTitle (ctx) {
    const title = await getTitleFromDb(ctx.query.title);
    ctx.body = title;
}

static async registerTitle (ctx) {
    const title = await createTitleInDb(ctx.request.body);
    ctx.body = title;
}