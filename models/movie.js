const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MovieSchema = new Schema({
    title: String,
    year: Number,
    released: String,
    genre: String,
    director: String,
    actors: String,
    plot: String,
    ratings: Number 
});

module.exports = mongoose.model('Movie', MovieSchema);